(ns japanpoddl.db
  (:require [taoensso.carmine :as car :refer (wcar)])
  )

(def server1-conn {:pool {} :spec {:host "127.0.0.1" :port 6379}}) ; See `wcar` docstring for opts
(defmacro wcar* [& body] `(car/wcar server1-conn ~@body))

;file-name-key is the filename will be the key
(defn add-link-db [file-name-key dialog-link lesson-notes-link]
  (wcar* (car/select 4))
  (wcar* (car/set file-name-key {:dialog-link dialog-link :lesson-notes-link lesson-notes-link :title file-name-key}))
  )

(defn get-one-key [key-name]
  (wcar* (car/select 4))
  (wcar* (car/get key-name))
  )

(defn get-all-unfinished-keys []
  (wcar* (car/select 4))
  (let [all-keys (wcar* (car/keys "*"))]
    (map #(if (= ((wcar* (car/get %)) :done) false)
            %
            ) all-keys)
    )
  )

(defn get-all-finished []
  (wcar* (car/select 4))
  (wcar* (car/smembers "finished"))
  )

(defn all-finished-to-unfinished []
  (wcar* (car/select 4))
  (for [one-key (wcar* (car/smembers "finished"))]
    (wcar* (car/smove "finished" "unfinished" one-key))
    )
  )

(defn get-all-unfinished []
  (wcar* (car/select 4))
  (wcar* (car/smembers "unfinished"))
  )

(defn add-to-set-finished [the-key]
  (wcar* (car/select 4))
  (wcar* (car/srem "unfinished" the-key))
  (wcar* (car/sadd "finished" the-key))
  )

(defn add-to-set-finished-season [category-url]
  (wcar* (car/select 4))
  (wcar* (car/sadd "finished-season" category-url))
  )

(defn delete-all-finished-seasons []
  (wcar* (car/select 4))
  (for [one-member (wcar* (car/smembers "finished-season"))]
    (wcar* (car/srem "finished-season" one-member)))
  )

(defn delete-all-unfinished []
  (wcar* (car/select 4))
  (for [one-member (wcar* (car/smembers "unfinished"))]
    (do
      (wcar* (car/srem "unfinished" one-member))
      (wcar* (car/del one-member))
      )

    )
  )

(defn delete-all-entries []
  (wcar* (car/select 4))
  (for [one-key (wcar* (car/keys "*"))]
    (wcar* (car/del one-key))
    )
  )

(defn get-all-finished-seasons []
  (wcar* (car/select 4))
  (wcar* (car/smembers "finished-season"))
  )

(defn add-to-set-unfinished [the-key]
  (wcar* (car/select 4))
  (wcar* (car/sadd "unfinished" the-key))
  )

(defn key-is-member? [key-name set-name]
  (wcar* (car/select 4))
  (if (= 1 (wcar* (car/sismember set-name key-name)))
    true
    false
    )
  )

(defn key-exists? [the-key]
  (wcar* (car/select 4))
  (if (= 1 (wcar* (car/exists the-key)))
    true
    false
    )
  )
