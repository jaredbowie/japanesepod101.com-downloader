(ns japanpoddl.httpheaders)

(def jpod-cookie-store (clj-http.cookies/cookie-store))

(def base-japanpod-request
  {:headers
   {"Accept" "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
    "Accept-Encoding" "gzip,deflate,sdch"
    "Accept-Language" "en-US,en;q=0.8"
    "Cache-Control" "max-age=0"
    "Connection" "keep-alive"
  ;  "Host" "www.japanesepod101.com"
  ;  "Origin" "http://www.japanesepod101.com"
    "Referer" "http://www.japanesepod101.com/"
    "User-Agent" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36"
    }
   :cookie-store jpod-cookie-store
   }
  )
