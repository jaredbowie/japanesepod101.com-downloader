(ns japanpoddl.getlinks
 (:require [clj-http.client :as client]
           [japanpoddl.mailinnator :refer [email-main-get-full-activation rand-email-gen]]
            ;[taoensso.carmine :as car :refer (wcar)]
            ;[clojure.java.io :as io]
            [japanpoddl.db :as db]
            [japanpoddl.httpheaders :refer [base-japanpod-request]]
            [japanpoddl.signup :refer [sign-up-and-get-activation-link open-activation-pass-offer]]
            )
 )
                                        ;structure
                                        ;Audio Lessons
                                        ;-Levels
                                        ;--Seasons
                                        ;---Lessons


(defn find-pdf
  "find the lesson plan pdf on an individual lesson page"
  [http-resp-body]
  ;(println http-resp-body)
  (let [url-start "http://www.japanesepod101.com/pdfs/"
        url-mid (second (re-find #"/pdfs/([\w_]+jpod101\.pdf)" http-resp-body))]
    (str url-start url-mid)))


(defn find-mp3
  "find the dialog mp3 on the individual lesson page"
  [http-resp-body]
  (second (re-find #"(http://media\.libsyn\.com/media/japanesepod101/[\w_]+jpod101_dialog\.mp3)" http-resp-body)))

(defn reg-ex-test
  "find the complete lesson url"
  [http-body]
  (let [lesson-url-pattern (re-pattern "<a href=\\\"http://www\\.japanesepod101\\.com/\\d+/\\d+/\\d+/[\\w-]+/\\\"")]
    (re-seq lesson-url-pattern http-body)
        ))


(defn find-season-links [http-resp-body]
  (println "find-season-links: finding season links")
  ;<a class=\"ill-season-title\" href=\"http://www.japanesepod101.com/index.php?cat=9\">Introduction</a>
  (let [matches (re-seq #"<a class=\"ill-season-title\" href=\"http://www\.japanesepod101\.com/index\.php\?cat=(\d+)\">[\w \'\-\:\/]+<" http-resp-body)
        all-season-links (vec (map #(str "http://www.japanesepod101.com/index.php?cat=" (second %)) matches))]
    (println (str "all-sesson-links" all-season-links))
    all-season-links
    )
  )

(defn one-lesson-scan [one-lesson-url]
  (println (str "one-lesson-url" one-lesson-url))
  (let [http-resp ((client/get one-lesson-url base-japanpod-request) :body)
        the-title (clojure.string/replace one-lesson-url #"http://www\.japanesepod101\.com/\d+/\d+/\d+/" "")
        the-mp3 (find-mp3 http-resp)
        the-pdf (find-pdf http-resp)]
    (db/add-link-db the-title the-mp3 the-pdf)
    (println (str "added " the-title ":" "mp3: " the-mp3 "pdf: " the-pdf))
    (db/add-to-set-unfinished the-title)
    (println "added to unfinished")
    ))

(defn season-scan
  "go through one season and return links for each lessson"
  [one-season-link]
  (do
    (println (str "first all-seasons-links" (first one-season-link))) ; really need to figure out why i'm doing first
    (let [lesson-url-pattern (re-pattern "<a href=\\\"(http://www\\.japanesepod101\\.com/\\d+/\\d+/\\d+/[\\w-]+)/\\\"")
          http-resp-body ((client/get (first one-season-link) base-japanpod-request) :body)]
      (re-seq lesson-url-pattern http-resp-body)
      )
    )
  )

(defn handle-one-lesson-link [one-lesson-link]
  (println (str "one-lesson-link" one-lesson-link))
  (let [the-title (clojure.string/replace one-lesson-link #"http://www\.japanesepod101\.com/\d+/\d+/\d+/" "")] ;check that it's not in the database
    (if (false? (db/key-exists? the-title))
      (do
        (println (str "Checking " the-title))
        (doall (one-lesson-scan one-lesson-link))) ;add to database with one-lesson-scan
      (println (str the-title " is already in the db"))
      )
    )
  )

(defn handle-one-season-link [one-season-link]
  (println (str "one-season-link " one-season-link))
  (if (false? (db/key-is-member? one-season-link "finished-seasons")) ;check that the season is not already in the database
    (do
      (println "one-season-link")
      (let [all-lesson-links-for-one-season (doall (season-scan [one-season-link]))] ;find all the lessons
        (doall (for [one-lesson-link all-lesson-links-for-one-season] ; for each lesson
                 (doall (handle-one-lesson-link (second one-lesson-link)))
                 ))

        ))
    (println (str "skipped " one-season-link "already in seasons finished db"))
    ))

(defn getlinks-main
  "find all links and add them to the database appropriately"
  []
  (println "Get activation link")
  (let [activation-link (sign-up-and-get-activation-link (rand-email-gen 10 "bobmail.info"))] ;returns activation link string
    (println "Open Activation and pass offer")
    (open-activation-pass-offer activation-link))
    (let [first-page-http-resp-body ((client/get "http://www.japanesepod101.com/index.php?cat=Introduction" base-japanpod-request) :body)]
      (println "Getting Season Links")
      (let [all-season-links (doall (find-season-links first-page-http-resp-body))] ;find all seasons
        (doall (for [one-season-link all-season-links] ;for each season
                 (do
                   (println one-season-link)
                   (handle-one-season-link one-season-link)
                                        ;maybe not the right link........
                   (db/add-to-set-finished-season one-season-link)) ;finished looping through each lesson of one season, so add that season done to database
                 ))

              ))
    (println "Done")
          )
