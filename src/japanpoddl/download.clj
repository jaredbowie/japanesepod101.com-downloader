(ns japanpoddl.download
  (:require [clj-http.client :as client]
            [japanpoddl.mailinnator :refer [rand-email-gen]]
            ;[taoensso.carmine :as car :refer (wcar)]
            [clojure.java.io :as io]
            [japanpoddl.db :as db]
            [japanpoddl.httpheaders :refer [base-japanpod-request]]
            [japanpoddl.signup :refer [sign-up-and-get-activation-link open-activation-pass-offer log-out]]
            [me.raynes.fs :as fs]
            )
  )

(def folder-to-use "/home/jared/japanpodall/")

(defn download-file-http []
  (assoc base-japanpod-request
    :as :byte-array
    )
  )

(defn create-proper-file-name [one-url starting-folder title]
  (if (re-find #"http\://japanesepod101\.com/pdfs/" one-url)
    (let [file-name (clojure.string/replace one-url #"http\://japanesepod101\.com/pdfs/" "")
          full-directory-and-file-name (str starting-folder title "/" file-name)]
      full-directory-and-file-name
      )
    (let [file-name (clojure.string/replace one-url #"http\://media\.libsyn\.com/media/japanesepod101/" "")
          full-directory-and-file-name (str starting-folder title "/" file-name)]
      full-directory-and-file-name
      )
    )
  )

(defn download-file [one-url starting-folder title]
  (println (str "downloading from " (clojure.string/replace one-url #"http\://japanesepod101\.com" "http://www.japanesepod101.com")))
  (let [temp-hacked-url (clojure.string/replace one-url #"http\://japanesepod101\.com" "http://www.japanesepod101.com")
        http-resp (client/get temp-hacked-url (download-file-http))
        ]
     (with-open [w (clojure.java.io/output-stream (create-proper-file-name one-url starting-folder title))]
     (.write w (:body http-resp)))
    )
  )

(defn download-all-files [download-map starting-folder]
  (println (str "download map" download-map))
  (fs/mkdir (str starting-folder (download-map :title)))
  (if (not= (download-map :dialog-link) nil)
    (download-file (download-map :dialog-link) starting-folder (download-map :title)))
  (if (not= (download-map :lesson-notes-link) "http://japanesepod101.com/pdfs/")
    (download-file (download-map :lesson-notes-link) starting-folder (download-map :title)))

  )
(defn core-download-all-links []
  (let [all-keys (db/get-all-unfinished)
        the-count (count all-keys)
        how-many-partitions (/ the-count 10)
        partitioned (map #(vec %) (partition-all 10 all-keys))
        ]
    ;(println partitioned)
    (doall (for [key-group partitioned]
             (do
               ;(println (clj-http.cookies/get-cookies jpod-cookie-store))
               (println "Get activation link")
               (let [activation-link (doall (sign-up-and-get-activation-link (rand-email-gen 10 "bobmail.info")))] ;returns activation link string
                 (println "Open Activation and pass offer")
                 (open-activation-pass-offer activation-link)
                 (doall (for [one-key key-group]
                          (do
                            (println (str "downloading " one-key))
                            (doall (download-all-files (db/get-one-key one-key) folder-to-use))
                            (println (str "adding " one-key " to finished"))
                            (db/add-to-set-finished one-key)))))
               (log-out))
             ))))

(defn part-play []
  (let [all-keys (db/get-all-unfinished)]
    (first (map #(vec %) (partition-all 10 all-keys)))
    )
  )
