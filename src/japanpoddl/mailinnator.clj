(ns japanpoddl.mailinnator
  (:require [clj-webdriver.taxi :as taxi]
            [clj-http.client :as client]
           ; [clojure.data.json :as json]
           ; [clj-time.core :as t]
            )
  )


(defn rand-email-gen [email-base-length email-tail]
  (let [range1 (range 48 58)
        ;range2 (range 65 91)
        range3 (range 97 123)
        range-combo (concat range1 range3)
        as-chars (map #(char %) range-combo)
        ]
                                        ;(char (rand-nth (range 48 58)))
    (str (apply str (for [x (range 10)]
                      (rand-nth as-chars)
                      )) "@" email-tail)))

(defn mail-login [email-address]
  (let [email-input (taxi/find-element {:tag :input :id "inboxfield"})
        check-it-button (taxi/find-element {:tag :btn :class "btn btn-success"})
        ]
    (taxi/send-keys email-input email-address)
    (taxi/click check-it-button)
    )
  )

(defn display-all-mails []
  (let [all-froms (taxi/find-elements {:tag :div :class "from ng-binding" :style "width:213px;float:left;"})
        all-subjects (taxi/find-elements {:tag :div :class "subject ng-binding" :style "width:323px;float: left;"})
        ]
    (map #(println (str ("From:  ") (taxi/text %) "- Subject:  " (taxi/text %2))) all-froms all-subjects)
    )
  )

(defn find-email-url-part []
  (let [;email-to-open (taxi/find-element {:text subject})
        emails-to-open (taxi/find-elements {:tag :a})
        element-that-matches  (first (filter #(not (nil? (re-find #"Activate Your JapanesePod101" (taxi/attribute % :text)))) emails-to-open))
        onclick-for-match (taxi/attribute element-that-matches :onclick)
        ]
    (second (re-find #"\'([\w-]+)\'" onclick-for-match))
    )
  )

(defn email-main-get-full-activation [email-address]
  (taxi/set-driver! {:browser :chrome} "http://www.mailinator.com/")
  (mail-login email-address)
  ;(display-all-mails)
  (Thread/sleep 5000)
  (let [part-url (find-email-url-part)
        full-url (str "http://www.mailinator.com/rendermail.jsp?msgid=" part-url "&amp;time=1397799012015")
        http-resp  ((client/get full-url) :body)
        ] ;time can be anything
    (taxi/quit)
    (second (re-find #"\<a href\=\"(http\://www.japanesepod101.com/member/signup\.php\?[\w&#;_]+)\">Click here</a>" http-resp))
    )
  )
