(ns japanpoddl.signup
  (:require
   [clj-http.client :as client]
   ;[japanpoddl.mailinnator :refer [email-main-get-full-activation]]
   ;[taoensso.carmine :as car :refer (wcar)]
   ;[clojure.java.io :as io]
   [japanpoddl.db :as db]
   [japanpoddl.httpheaders :refer [base-japanpod-request]]
   [japanpoddl.mailinnator :refer [email-main-get-full-activation]]
   )
  )



(defn sign-up-middleware [email]
  (assoc base-japanpod-request
    :query-params {"validate" "true"}
    :form-params {"clickpath" "|/"
                  "price_group" ""
                  "signup_level_selected" "Beginner"
                  "product_id" "1"
                  "pasys_id" "free"
                  "controlgroup" ""
                  "do_payment" "1"
                  "signup_source" "signup_home_quick.php"
                  "login" "_"
                  "email" email
                  } ))

(defn log-out []
  (client/get "http://www.japanesepod101.com/member/logout.php" base-japanpod-request)
  )

(defn first-page-load []
  (client/get "http://www.japanesepod101.com/" base-japanpod-request))

(defn sign-up [email-to-sign-up]
  (client/post "http://www.japanesepod101.com/cart/signup_8/signup_home.php" (sign-up-middleware email-to-sign-up))
  )


(defn open-activation-pass-offer [activation-link]
  (client/get activation-link base-japanpod-request)
  (client/get "http://www.japanesepod101.com/member/plugins/payment/free/thanks.php" base-japanpod-request)
  )

;            &#61; =
;            &amp; &
(defn format-url [url]
  (let [first-replace (clojure.string/replace url #"&#61;" "=")
        second-replace (clojure.string/replace first-replace #"&amp;" "&")
        ;third-replace (clojure.string/replace second-replace #"member_id_exists\=0" "member_id_exists=1")
        ]
    second-replace
    )
  )

(defn sign-up-and-get-activation-link [email-to-sign-up]
  (println "Starting Signup")
  (println "Loading First Page")
  (first-page-load)
  (println (str "signing up as a beginner with " email-to-sign-up))
  (sign-up email-to-sign-up)
  (println "getting activation link")
  (let [activation-link (email-main-get-full-activation email-to-sign-up)
        formatted-activation-link (format-url activation-link)
        ]
    (println (str "Activation Link" formatted-activation-link))
    formatted-activation-link
  ))
